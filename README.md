
## Publish

```
npm config set registry https://registry.npmjs.org/
npm login
yarn build
npm version patch
npm publish --access public
```

To Private repo

Variables NPM_USER,  NPM_PASS, NPM_EMAIL should be set

```
npm config set registry http://verdaccio.dodonum.com:4873/
npm i -g npm-cli-login
NPM_REGISTRY=$PRIVATE_NPM_REGISTRY NPM_USER=$PRIVATE_NPM_USER NPM_PASS=$PRIVATE_NPM_PASS NPM_EMAIL=$PRIVATE_NPM_EMAIL npm-cli-login
yarn build
npm version patch
npm publish
```

## License

MIT © [damir-manapov](https://github.com/damir-manapov)
