import {AxiosPromise} from 'axios';
import {fromPairs, toPairs} from 'lodash';
import {
  EmailNotConfirmed,
  EErrorTypes,
  UserLocked,
} from '@instants/browser-auth';
import {IHttpResponse, EHttpResponseStatuses} from './types';
import qs from 'qs';
import log from 'loglevel';
import {IList, IReportResult, IReportResultWithTotal} from '@instants/core';
import {Fail} from './errors/Fail';
import {Err} from './errors/Err';

export const checkStatus = (response: IHttpResponse<any>) => {
  if (response.status !== EHttpResponseStatuses.Success) {
    switch (response.status) {
    case EErrorTypes.UserLocked:
      log.warn('userLocked');

      throw new UserLocked(response.data);
    case EErrorTypes.EmailNotConfirmed:
      log.warn('emailNotConfirmed');

      throw new EmailNotConfirmed(response.data);
    case EHttpResponseStatuses.Fail:
      log.error(
        `Fail on request, status: ${response.status}, err: ${response.message}`,
      );

      throw new Fail(response.message, response.data);

    case EHttpResponseStatuses.Error:
      log.error(
        `Error on request, status: ${response.status}, err: ${response.message}`,
      );

      throw new Err(response.message, response.data);
    default:
      log.error(
        `err on request, status: ${response.status}, err: ${response.message}`,
      );

      throw response.message;
    }
  }
};

export const getRequestData = async function <D>(
  promise: AxiosPromise<IHttpResponse<D>>,
): Promise<D> {
  const {data} = await promise;

  checkStatus(data);

  return data.data;
};

export const getRequestDataWithPageInfo = async function <D>(
  promise: AxiosPromise<IHttpResponse<D[]>>,
): Promise<IList<D>> {
  const {data} = await promise;

  checkStatus(data);

  return {
    list: data.data,
    pageInfo: data.pageInfo ?
      data.pageInfo :
      {
        currentPage: 1,
        elementsOnPage: data?.data?.length ?? 0,
        pageCount: 1,
      },
  };
};

export const getReportData = async function <Row>(
  promise: AxiosPromise<IHttpResponse<{ rows: Row[] }>>,
): Promise<IReportResult<Row>> {
  const {data: axiosData} = await promise;

  checkStatus(axiosData);

  return {
    pageInfo: axiosData.pageInfo ?
      axiosData.pageInfo :
      {
        currentPage: 1,
        elementsOnPage: axiosData.data?.rows?.length ?? 0,
        pageCount: 1,
      },
    rows: axiosData?.data?.rows ?? [],
  };
};

export const getReportDataWithTotal = async function <Row>(
  promise: AxiosPromise<IHttpResponse<{ rows: Row[]; total: Partial<Row> }>>,
): Promise<IReportResultWithTotal<Row>> {
  const {data: axiosData} = await promise;

  checkStatus(axiosData);

  return {
    pageInfo: axiosData.pageInfo ?
      axiosData.pageInfo :
      {
        currentPage: 1,
        elementsOnPage: axiosData.data?.rows?.length ?? 0,
        pageCount: 1,
      },
    rows: axiosData?.data?.rows ?? [],
    total: axiosData?.data?.total,
  };
};

export const withTimeout = function <D>(data: D, timeout = 2000): Promise<D> {
  return new Promise<D>((resolve) => {
    setTimeout(() => resolve(data), timeout);
  });
};

export const urlStr = (params: any): string => {
  const filteredParams = fromPairs(
    toPairs(params).filter(
      ([, value]) => Boolean(value) || value === 0 || typeof value === 'boolean',
    ),
  );

  return qs.stringify(filteredParams, {
    arrayFormat: 'repeat',

    // filter: (prefix: string, value: any) => {
    //   log.info('prefix', prefix, 'value', value, Boolean(value));

    //   // return value ? value : undefined;
    //   return value;
    // },
  });
};

// make ?query or empty string
export const getQuery = (request?: any): string => (request ? '?' + urlStr(request) : '');
