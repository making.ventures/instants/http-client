import {IPageInfo} from '@instants/core';

export enum EHttpResponseStatuses {
  Success = 'success',
  Error = 'error',
  Fail = 'fail',
}
export interface IHttpResponse<TData> {
  status: EHttpResponseStatuses | string;
  data: TData;
  message?: string;
  pageInfo?: IPageInfo;
}
