import {EHttpResponseStatuses} from '../types';

export class Fail extends Error {
  public type = EHttpResponseStatuses.Fail;

  public message = '';

  public data?: Record<string, any> = {};

  constructor(message = 'no message', data?: Record<string, any>) {
    super(`Fail: ${message}`);

    this.message = message;
    this.data = data;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, Fail.prototype);
  }
}
