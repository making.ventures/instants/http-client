import {EHttpResponseStatuses} from '../types';

export class Err extends Error {
  public type = EHttpResponseStatuses.Error;

  public message = '';

  public data?: Record<string, any> = {};

  constructor(message = 'no message', data?: Record<string, any>) {
    super(`Error: ${message}`);

    this.message = message;
    this.data = data;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, Err.prototype);
  }
}
