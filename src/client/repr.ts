import {AxiosInstance} from 'axios';
import {reprPrePath, listPrePath} from './shared';
import {getM, getMWithPageInfo, defaultListRequest} from './ui';
import {
  IUIRepresentation,
  IList,
  IListRequest,
  IHttpPath,
  EHttpMethod,
} from '@instants/core';
import {ICache} from '../types';
import {getQuery} from "../utils";

export const reprListPath = (
  basePath: string,
  request?: IListRequest,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${reprPrePath()}/${listPrePath(request)}`,
});
export const reprList = async <T extends number | string = number>(
  getAx: () => AxiosInstance,
  basePath: string,
  request?: IListRequest,
): Promise<IList<IUIRepresentation<T>>> =>
  getMWithPageInfo(
    getAx,
    'reprList',
    reprListPath(
      basePath,
      request ? {...defaultListRequest, ...request} : defaultListRequest,
    ).path,
    request,
  );

export const reprGetPath = (basePath: string, id: string, request?: any): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${reprPrePath()}/entity/${id}` + getQuery(request),
});
export const reprGet = async <T extends number | string = number>(
  getAx: () => AxiosInstance,
  basePath: string,
  id: T,
  cache?: ICache<IUIRepresentation<T>>,
  request?: any,
): Promise<IUIRepresentation<T>> => {
  if (cache) {
    // log.warn(`reprGet there is getCache, id: ${id}`);
    const strId = id.toString();

    if (!cache.has(strId)) {
      // log.warn(`reprGet, id: ${id}. There is no data in cache, loading`);
      const value = (await getM(
        getAx,
        'reprGet',
        reprGetPath(basePath, String(id), request).path,
      )) as IUIRepresentation<T>;

      // log.warn(`reprGet, id: ${id}. Loaded. value: ${jstr(value)}`);
      cache.set(strId, value);

      // log.warn(
      //   `reprGet, id: ${id}. Put to cache, data in cache: ${jstr(
      //     cache.get(strId),
      //   )}`,
      // );
    }

    return cache.get(strId) as IUIRepresentation<T>;
  }

  // log.warn(`reprGet there is no getCache, id: ${id}`);

  return getM(getAx, 'reprGet', reprGetPath(basePath, String(id), request).path);
};
