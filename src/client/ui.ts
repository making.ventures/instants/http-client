/* eslint-disable indent */
import {AxiosInstance, AxiosError} from 'axios';
import log from 'loglevel';
import {
  uiPrePath,
  listPrePath,
  basicPrePath,
  getRequestPrePath,
} from './shared';
import {
  getReportData,
  getReportDataWithTotal,
  getRequestData,
  getRequestDataWithPageInfo,
  getQuery,
} from '../utils';
import {kebab} from 'case';
import {
  jstr,
  IBaseEntity,
  RequiredId,
  IListRequest,
  TListRequest,
  IList,
  IReportResult,
  IHttpPath,
  EHttpMethod,
  TAfterRead,
  IReportResultWithTotal,
} from '@instants/core';

export const getAxiousFunc = (
  getAx: () => AxiosInstance,
  method: EHttpMethod,
) => {
  switch (method) {
    case EHttpMethod.GET:
      return getAx().get;
    case EHttpMethod.POST:
      return getAx().post;
    case EHttpMethod.PUT:
      return getAx().put;
    case EHttpMethod.DELETE:
      return getAx().delete;
    default:
      throw new Error(`Unexpected method, method: ${method}`);
  }
};

export const method = async <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  name: string,
  method: EHttpMethod,
  path: string,
  request?: TRequest,
  afterRead: (e: TResponse) => TResponse = (e) => e,
): Promise<TResponse> => {
  return getRequestData<RequiredId<TResponse>>(
    getAxiousFunc(getAx, method)(path, request),
  )
    .then((data) => afterRead(data))
    .catch((error) => {
      log.error(
        `http client method: ${name}, ${method} path: ${path}. request: ${jstr(
          request,
        )}. err: ${error}`,
      );
      throw error;
    });
};

export const listWithPageInfo = async <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  name: string,
  method: EHttpMethod,
  path: string,
  request?: TRequest,
  afterRead: (e: TResponse) => TResponse = (e) => e,
): Promise<IList<TResponse>> => {
  return getRequestDataWithPageInfo<RequiredId<TResponse>>(
    getAxiousFunc(getAx, method)(path, request),
  )
    .then((result) => {
      return {
        pageInfo: {},
        ...result,
        list:
          result && result.list ? result.list.map((el) => afterRead(el)) : [],
      };
    })
    .catch((error) => {
      log.error(
        `http client method: ${name}, ${method} path: ${path}. request: ${jstr(
          request,
        )}. err: ${error}`,
      );
      throw error;
    });
};

export const getReport = async <TRequest, Row>(
  getAx: () => AxiosInstance,
  name: string,
  path: string,
  request?: TRequest,
  afterRead: (e: Row) => Row = (e) => e,
): Promise<IReportResult<Row>> => {
  return getReportData<Row>(
    getAxiousFunc(
      getAx,
      EHttpMethod.GET,
    )(path + getQuery(request)),
  )
    .then((result) => {
      return {
        pageInfo: {},
        ...result,
        rows:
          result && result.rows ? result.rows.map((el) => afterRead(el)) : [],
      };
    })
    .catch((error) => {
      log.error(
        `http client method: ${name}, ${method} path: ${path}. request: ${jstr(
          request,
        )}. err: ${error}`,
      );
      throw error;
    });
};

export const getReportWithTotal = async <TRequest, Row>(
  getAx: () => AxiosInstance,
  name: string,
  path: string,
  request?: TRequest,
  afterRead: (e: Row) => Row = (e) => e,
): Promise<IReportResultWithTotal<Row>> => {
  return getReportDataWithTotal<Row>(
    getAxiousFunc(
      getAx,
      EHttpMethod.GET,
    )(path + getQuery(request)),
  )
    .then((result) => {
      return {
        pageInfo: {},
        ...result,
        rows:
          result && result.rows ? result.rows.map((el) => afterRead(el)) : [],
      };
    })
    .catch((error) => {
      log.error(
        `http client method: ${name}, ${method} path: ${path}. request: ${jstr(
          request,
        )}. err: ${error}`,
      );
      throw error;
    });
};

export const getDataWithQuery = async <TQuery, TDate>(
  getAx: () => AxiosInstance,
  name: string,
  path: string,
  query?: TQuery,
): Promise<TDate> => {
  log.info(`getRequestData, name: ${name}, path: ${path}, request: ${jstr(query)}`);

  return getAxiousFunc(
    getAx,
    EHttpMethod.GET,
  )(path + getQuery(query))
    .then((result: { data: TDate }) => (result.data))
    .catch((error: AxiosError) => {
      log.error(
        `http client method: ${name}, ${method} path: ${path}. request: ${jstr(
          query,
        )}. err: ${error}`,
      );
      throw error;
    });
};

export const getM = async <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  name: string,
  path: string,
  request?: TRequest,
  afterRead: (e: TResponse) => TResponse = (e) => e,
): Promise<TResponse> =>
  method(getAx, name, EHttpMethod.GET, path, request, afterRead);

export const getMWithPageInfo = async <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  name: string,
  path: string,
  request?: TRequest,
  afterRead: (e: TResponse) => TResponse = (e) => e,
): Promise<IList<TResponse>> =>
  listWithPageInfo(getAx, name, EHttpMethod.GET, path, request, afterRead);

export const post = async <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  name: string,
  path: string,
  request?: TRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<TResponse> =>
  method(getAx, name, EHttpMethod.POST, path, request, afterRead);

export const del = async <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  name: string,
  path: string,
  request?: TRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<TResponse> =>
  method(getAx, name, EHttpMethod.DELETE, path, request, afterRead);

export const put = async <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  name: string,
  path: string,
  request?: TRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<RequiredId<TResponse>> =>
  method(getAx, name, EHttpMethod.PUT, path, request, afterRead);

export const uiListPath = <ListRequest extends IListRequest>(
  basePath: string,
  request?: ListRequest,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${uiPrePath()}/${listPrePath(request)}`,
});

// export const uiList = async <
//   TUIForList,
//   TUIForListRequest extends IListRequest
// >(
//   getAx: () => AxiosInstance,
//   basePath: string,
//   request?: TUIForListRequest,
//   afterRead: TAfterRead<RequiredId<TUIForList>> = (e) => e,
// ): Promise<RequiredId<TUIForList>[]> =>
//   getM<TUIForListRequest, RequiredId<TUIForList>[]>(
//     getAx,
//     'uiList',
//     uiListPath(
//       basePath,
//       request ?
//         {elementsOnPage: 1000000, ...request} :
//         {elementsOnPage: 1000000},
//     ).path,
//     undefined,
//     (list) => list.map((el) => afterRead(el)),
//   );

export const uiList = async <
  TUIForList,
  TUIForListRequest extends IListRequest
>(
  getAx: () => AxiosInstance,
  basePath: string,
  request?: TUIForListRequest,
  afterRead: TAfterRead<RequiredId<TUIForList>> = (e) => e,
): Promise<IList<RequiredId<TUIForList>>> =>
  listWithPageInfo<TUIForListRequest, RequiredId<TUIForList>>(
    getAx,
    'uiList',
    EHttpMethod.GET,
    uiListPath(
      basePath,
      request ?
        {elementsOnPage: 1000000, ...request} :
        {elementsOnPage: 1000000},
    ).path,
    undefined,
    (el) => {
      // log.warn(`el: ${jstr(el)}`);

      return afterRead(el);

      // return {
      //   ...result,
      //   list: result.list ? result.list.map(el => afterRead(el)) : [],
      // };
    },
  );

export const listPath = (
  basePath: string,
  request?: IListRequest,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${basicPrePath()}/${listPrePath(request)}`,
});

// export const list = async <TUIForList>(
//   getAx: () => AxiosInstance,
//   basePath: string,
//   request?: IListRequest,
//   afterRead: TAfterRead<TUIForList> = (e) => e,
// ): Promise<RequiredId<TUIForList>[]> =>
//   method<IListRequest, RequiredId<TUIForList>[]>(
//     getAx,
//     'list',
//     EHttpMethod.GET,
//     listPath(
//       basePath,
//       request ?
//         {elementsOnPage: 1000000, ...request} :
//         {elementsOnPage: 1000000},
//     ).path,
//     request,
//     (list) => list.map((el: RequiredId<TUIForList>) => afterRead(el)),
//   );

export const defaultListRequest = {active: true, elementsOnPage: 1000000};

export const list = async <TUIForList>(
  getAx: () => AxiosInstance,
  basePath: string,
  request?: TListRequest<any>,
  afterRead: TAfterRead<TUIForList> = (e) => e,
): Promise<IList<RequiredId<TUIForList>>> =>
  getMWithPageInfo<IListRequest, RequiredId<TUIForList>>(
    getAx,
    'list',
    listPath(
      basePath,
      request ? {...defaultListRequest, ...request} : defaultListRequest,
    ).path,
    request,
    (el: RequiredId<TUIForList>) => afterRead(el),
  );

export const uiGetPath = (basePath: string, id: string): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${uiPrePath()}/entity/${String(id)}`,
});
export const uiGet = async <T extends IBaseEntity<number>>(
  getAx: () => AxiosInstance,
  basePath: string,
  id: number,
  afterRead: TAfterRead<RequiredId<T>> = (e) => e,
): Promise<RequiredId<T>> =>
  getM(
    getAx,
    'uiGet',
    uiGetPath(basePath, String(id)).path,
    undefined,
    afterRead,
  );

export const uiGetStr = async <TUIGet extends IBaseEntity<string>>(
  getAx: () => AxiosInstance,
  basePath: string,
  id: string,
  afterRead: TAfterRead<RequiredId<TUIGet>> = (e) => e,
): Promise<RequiredId<TUIGet>> =>
  getM(
    getAx,
    'uiGet',
    uiGetPath(basePath, String(id)).path,
    undefined,
    afterRead,
  );

export const uiGetRequestPath = <Request>(
  basePath: string,
  request?: Request,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${uiPrePath()}/${getRequestPrePath(request)}`,
});
export const uiGetRequest = async <Request, T extends IBaseEntity<number>>(
  getAx: () => AxiosInstance,
  basePath: string,
  request?: Request,
  afterRead: TAfterRead<RequiredId<T>> = (e) => e,
): Promise<RequiredId<T>> =>
  getM(
    getAx,
    'uiGet',
    uiGetRequestPath(basePath, request).path,
    undefined,
    afterRead,
  );

// export const uiGetLangPath = (
//   basePath: string,
//   id: string,
//   lang: string,
// ): IHttpPath => ({
//   method: EHttpMethod.GET,
//   path: `${basePath}/${uiPrePath()}/entity/${id}/?lang=${encodeURIComponent(
//     JSON.stringify({lang}),
//   )}`,
// });
// export const uiGetLang = async <T extends IBaseEntity, TUIGet>(
//   getAx: () => AxiosInstance,
//   basePath: string,
//   id: T['id'],
//   lang: string,
//   afterRead: TAfterRead<RequiredId<TUIGet>> = e => e,
// ): Promise<RequiredId<TUIGet>> =>
//   getM(
//     getAx,
//     'uiGetLang',
//     uiGetLangPath(basePath, String(id), lang).path,
//     undefined,
//     afterRead,
//   );

export const getPath = (basePath: string, id: string): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${basicPrePath()}/entity/${String(id)}`,
});

export const get = async <T extends IBaseEntity<number>>(
  getAx: () => AxiosInstance,
  basePath: string,
  id: number,
  afterRead: TAfterRead<RequiredId<T>> = (e) => e,
): Promise<RequiredId<T>> =>
  getM(getAx, 'get', getPath(basePath, String(id)).path, undefined, afterRead);

export const getStr = async <T extends IBaseEntity<string>>(
  getAx: () => AxiosInstance,
  basePath: string,
  id: string,
  afterRead: TAfterRead<RequiredId<T>> = (e) => e,
): Promise<RequiredId<T>> =>
  getM(getAx, 'get', getPath(basePath, String(id)).path, undefined, afterRead);

export const uiDeletePath = (basePath: string, id: string): IHttpPath => ({
  method: EHttpMethod.DELETE,
  path: `${basePath}/${uiPrePath()}/entity/${String(id)}`,
});
export const uiDelete = async (
  getAx: () => AxiosInstance,
  basePath: string,
  id: number,
): Promise<number> =>
  del(getAx, 'uiDelete', uiDeletePath(basePath, String(id)).path);

export const uiDeleteStr = async (
  getAx: () => AxiosInstance,
  basePath: string,
  id: string,
): Promise<string> => del(getAx, 'uiDelete', uiDeletePath(basePath, id).path);

export const deletePath = (basePath: string, id: string): IHttpPath => ({
  method: EHttpMethod.DELETE,
  path: `${basePath}/${basicPrePath()}/entity/${String(id)}`,
});
export const deleteMethod = async (
  getAx: () => AxiosInstance,
  basePath: string,
  id: number,
): Promise<number> =>
  del(getAx, 'deleteMethod', deletePath(basePath, String(id)).path);

export const deleteMethodStr = async (
  getAx: () => AxiosInstance,
  basePath: string,
  id: string,
): Promise<string> =>
  del(getAx, 'deleteMethod', deletePath(basePath, String(id)).path);

export const uiUpdatePath = (basePath: string, id: string): IHttpPath => ({
  method: EHttpMethod.PUT,
  path: `${basePath}/${uiPrePath()}/entity/${id}`,
});
export const uiUpdate = async <
  TUIUpdate extends IBaseEntity<number | string>,
  TUIGet
>(
  getAx: () => AxiosInstance,
  basePath: string,
  entity: TUIUpdate,
  afterRead: TAfterRead<RequiredId<TUIGet>> = (e) => e,
): Promise<RequiredId<TUIGet>> =>
  put(
    getAx,
    'uiUpdate',
    uiUpdatePath(basePath, String(entity.id)).path,
    entity,
    afterRead,
  );

export const updatePath = (basePath: string, id: string): IHttpPath => ({
  method: EHttpMethod.PUT,
  path: `${basePath}/${basicPrePath()}/entity/${id}`,
});
export const update = async <T extends IBaseEntity<any>>(
  getAx: () => AxiosInstance,
  basePath: string,
  entity: T,
  afterRead: TAfterRead<RequiredId<T>> = (e) => e,
): Promise<RequiredId<T>> =>
  put(
    getAx,
    'update',
    updatePath(basePath, String(entity.id)).path,
    entity,
    afterRead,
  );

export const uiCreatePath = (basePath: string): IHttpPath => ({
  method: EHttpMethod.POST,
  path: `${basePath}/${uiPrePath()}/entity`,
});
export const uiCreate = async <TUICreate, TUIGet>(
  getAx: () => AxiosInstance,
  basePath: string,
  entity: TUICreate,
  afterRead: TAfterRead<RequiredId<TUIGet>> = (e) => e,
): Promise<RequiredId<TUIGet>> =>
  post(getAx, 'uiCreate', uiCreatePath(basePath).path, entity, afterRead);

export const createPath = (basePath: string): IHttpPath => ({
  method: EHttpMethod.POST,
  path: `${basePath}/${basicPrePath()}/entity`,
});
export const create = async <T>(
  getAx: () => AxiosInstance,
  basePath: string,
  entity: T,
  afterRead: TAfterRead<RequiredId<T>> = (e) => e,
): Promise<RequiredId<T>> =>
  post(getAx, 'create', createPath(basePath).path, entity, afterRead);

export const generalMethodPath = (
  basePath: string,
  methodName: string,
): IHttpPath => ({
  method: EHttpMethod.POST,
  path: `${basePath}/${uiPrePath()}/methods/${kebab(methodName)}`,
});
export const uiGeneralMethod = async <TUIRequest, TResponse>(
  getAx: () => AxiosInstance,
  basePath: string,
  methodName: string,
  request?: TUIRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<TResponse> =>
  post(
    getAx,
    'uiGeneralMethod',
    generalMethodPath(basePath, methodName).path,
    request,
    afterRead,
  );

export const generalGetMethodPath = <TUIRequest>(
  basePath: string,
  methodName: string,
  request?: TUIRequest,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${uiPrePath()}/methods/${kebab(methodName)}${getQuery(request)}`,
});

export const uiGeneralGetMethod = async <TUIRequest, TResponse>(
  getAx: () => AxiosInstance,
  basePath: string,
  methodName: string,
  request?: TUIRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<TResponse> =>
  getM(
    getAx,
    'uiGeneralGetMethod',
    generalGetMethodPath<TUIRequest>(basePath, methodName, request).path,
    request,
    afterRead,
  );
export const uiGeneralGetMethodWithPageInfo = async <TUIRequest, TResponse>(
  getAx: () => AxiosInstance,
  basePath: string,
  methodName: string,
  request?: TUIRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<IList<RequiredId<TResponse>>> =>
  getMWithPageInfo(
    getAx,
    'uiGeneralGetMethod',
    generalGetMethodPath<TUIRequest>(basePath, methodName, request).path,
    request,
    afterRead,
  );

export const entityMethodPath = (
  basePath: string,
  id: string,
  methodName: string,
): IHttpPath => ({
  method: EHttpMethod.POST,
  path: `${basePath}/${uiPrePath()}/entity/${id}/methods/${kebab(methodName)}`,
});
export const uiEntityMethod = async <T extends IBaseEntity,
  TUIRequest,
  TResponse>(
  getAx: () => AxiosInstance,
  basePath: string,
  id: T['id'],
  methodName: string,
  request?: TUIRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<TResponse> =>
  post(
    getAx,
    'uiEntityMethod',
    entityMethodPath(basePath, String(id), methodName).path,
    request,
    afterRead,
  );

export const uiEntityGetMethodPath = <TUIRequest>(
  basePath: string,
  id: string,
  methodName: string,
  request?: TUIRequest,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${uiPrePath()}/entity/${id}/methods/${kebab(
    methodName,
  )}${getQuery(request)}`,
});
export const uiEntityGetMethod = async <
  T extends IBaseEntity,
  TUIRequest,
  TResponse
>(
  getAx: () => AxiosInstance,
  basePath: string,
  id: T['id'],
  methodName: string,
  request?: TUIRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<TResponse> =>
  getM(
    getAx,
    'uiEntityGetMethod',
    uiEntityGetMethodPath<TUIRequest>(basePath, String(id), methodName, request)
      .path,
    request,
    afterRead,
  );
