import {IPathMetaHolder, IHttpPath} from '@instants/core';

export abstract class PathMetaHolder implements IPathMetaHolder {
  getPaths(): IHttpPath[] {
    return [];
  }
}
