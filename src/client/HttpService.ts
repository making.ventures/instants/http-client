import {AxiosInstance} from 'axios';
import {PathMetaHolder} from './PathMetaHolder';

export abstract class HttpService extends PathMetaHolder {
  public ax: AxiosInstance;

  public basePath: string;

  public constructor(basePath: string) {
    super();
    this.basePath = basePath;
    this.ax = undefined as any;
  }

  public initAx(ax: AxiosInstance) {
    this.ax = ax;
  }
}
