import {HttpService} from './HttpService';
import {reprGet as reprGetHttp, reprList as reprListHttp} from './repr';
import {
  IUIRepresentation,
  RequiredId,
  IList,
  IListRequest,
  IPathMetaHolder,
  IHttpPath,
  EHttpMethod,
  TGet,
  TList,
  IBaseUIReprService,
} from '@instants/core';

export abstract class ReprBaseService extends HttpService
  implements IBaseUIReprService, IPathMetaHolder {
  public constructor(basePath: string) {
    super(basePath);
  }

  public getPaths(): IHttpPath[] {
    const id = '1';

    return [
      {
        method: EHttpMethod.GET,
        path: `${this.basePath}/repr/entity/${id}`,
      },
      {
        method: EHttpMethod.GET,
        path: `${this.basePath}/repr/list`,
      },
    ];
  }

  public reprGet: TGet<IUIRepresentation> = async (id) => {
    return reprGetHttp(() => this.ax, this.basePath, id) as Promise<
      RequiredId<IUIRepresentation>
    >;
  };

  public reprList: TList<IListRequest, IUIRepresentation> = async (request) => {
    const r = request ?
      request :
      {
        elementsOnPage: 1000000,
      };

    // return reprListHttp(() => this.ax, this.basePath, r) as Promise<
    //   RequiredId<IUIRepresentation>[]
    // >;

    return reprListHttp(() => this.ax, this.basePath, r) as Promise<
      IList<RequiredId<IUIRepresentation>>
    >;
  };
}
