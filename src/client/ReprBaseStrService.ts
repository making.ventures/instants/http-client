import {HttpService} from './HttpService';
import {reprGet as reprGetHttp, reprList as reprListHttp} from './repr';
import {
  IUIRepresentation,
  RequiredId,
  IList,
  IListRequest,
  IPathMetaHolder,
  IHttpPath,
  EHttpMethod,
  TGetStr,
  TList,
  IBaseUIReprStrService,
} from '@instants/core';

export abstract class ReprBaseStrService extends HttpService
  implements IBaseUIReprStrService, IPathMetaHolder {
  public constructor(basePath: string) {
    super(basePath);
  }

  public getPaths(): IHttpPath[] {
    const id = '1';

    return [
      {
        method: EHttpMethod.GET,
        path: `${this.basePath}/repr/entity/${id}`,
      },
      {
        method: EHttpMethod.GET,
        path: `${this.basePath}/repr/list`,
      },
    ];
  }

  public reprGet: TGetStr<IUIRepresentation<string>> = async (id) => {
    return reprGetHttp(() => this.ax, this.basePath, id) as Promise<
      RequiredId<IUIRepresentation<string>>
    >;
  };

  public reprList: TList<IListRequest, IUIRepresentation<string>> = async (
    request,
  ) => {
    const r = request ?
      request :
      {
        elementsOnPage: 1000000,
      };

    return reprListHttp<string>(() => this.ax, this.basePath, r) as Promise<
      IList<RequiredId<IUIRepresentation<string>>>
    >;
  };
}
