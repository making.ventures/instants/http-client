import {IBaseEntity, RequiredId} from '@instants/core';

export interface IBaseServiceMethods<T extends IBaseEntity<number>> {
  consistencyCheck(entity: T): Promise<void>;
  beforeCreate(entity: T): Promise<T>;
  afterRead(entity: RequiredId<T>): RequiredId<T>;
  beforeUpdate(entity: T): Promise<T>;
  couldBeDelited(entity: T): Promise<void>;
}
