import {ReprBaseStrService} from './ReprBaseStrService';
import {
  getStr,
  getPath,
  create,
  createPath,
  deleteMethodStr,
  deletePath,
  listPath,
  update,
  updatePath,
  list,
} from './ui';
import {IBaseStrServiceMethods} from './IBaseStrServiceMethods';
import {
  IBaseEntity,
  RequiredId,
  IListRequest,
  IList,
  EOrderDirection,
  IPathMetaHolder,
  IHttpPath,
  IBaseCRUDStrService,
} from '@instants/core';

// tslint:disable: member-ordering
export abstract class BaseStrService<T extends IBaseEntity<string>>
  extends ReprBaseStrService
  implements
  IBaseCRUDStrService<T>,
  IBaseStrServiceMethods<T>,
  IPathMetaHolder {
  public constructor(basePath: string) {
    super(basePath);
    this.get = this.get.bind(this);
    this.create = this.create.bind(this);
    this.delete = this.delete.bind(this);
    this.list = this.list.bind(this);
    this.list = this.list.bind(this);
    this.update = this.update.bind(this);
    this.reprGet = this.reprGet.bind(this);
    this.reprList = this.reprList.bind(this);
  }

  // Hooks
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public async consistencyCheck(): Promise<void> { }

  public afterRead(entity: RequiredId<T>): RequiredId<T> {
    return entity;
  }

  public async beforeCreate(entity: T): Promise<T> {
    return entity;
  }

  public async beforeUpdate(entity: T): Promise<T> {
    return entity;
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public async couldBeDelited(): Promise<void> { }

  public getPaths(): IHttpPath[] {
    const id = '1';

    return [
      ...super.getPaths(),
      getPath(this.basePath, id),
      createPath(this.basePath),
      updatePath(this.basePath, id),
      deletePath(this.basePath, id),
      listPath(this.basePath),
    ];
  }

  public async get(id: string): Promise<RequiredId<T>> {
    return this.afterRead(await getStr<T>(() => this.ax, this.basePath, id));
  }

  public async create(entity: T): Promise<RequiredId<T>> {
    return this.afterRead(
      await create<T>(() => this.ax, this.basePath, entity),
    );
  }

  public async delete(id: string): Promise<string> {
    return deleteMethodStr(() => this.ax, this.basePath, id);
  }

  // public async list(request?: IListRequest): Promise<RequiredId<T>[]> {
  //   const base = await list<T>(() => this.ax, this.basePath, request);

  //   return base.map(this.afterRead.bind(this));
  // }

  public async list(request?: IListRequest): Promise<IList<RequiredId<T>>> {
    const base = await list<T>(() => this.ax, this.basePath, {
      sortDirection: EOrderDirection.DESC,
      sortField: 'id',
      ...request,
    });

    // if (typeof ((base as unknown) as Array<any>).length === 'number') {
    //   return {
    //     list: ((base as unknown) as Array<any>),
    //     pageInfo: {
    //       currentPage: 1,
    //       elementsOnPage: ((base as unknown) as Array<any>).length,
    //       pageCount: 1,
    //     },
    //   };
    // }

    return {
      pageInfo: {},
      ...base,
      list: base && base.list ? base.list.map(this.afterRead.bind(this)) : [],
    };
  }

  public async update(entity: T): Promise<RequiredId<T>> {
    return this.afterRead(
      await update<T>(() => this.ax, this.basePath, entity),
    );
  }
}
