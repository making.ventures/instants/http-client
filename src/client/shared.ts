import {kebab} from 'case';
import {IListRequest} from '@instants/core';
import {getQuery} from '../utils';

// Preparhs
export const entityMethodPrePath = (id: string, methodName: string): string =>
  `entity/${id}/methods/${kebab(methodName)}`;
export const uiPrePath = (): string => 'ui';
export const reprPrePath = (): string => 'repr';
export const basicPrePath = (): string => 'basic';
export const generalSubentityPrePath = (subentityName: string): string =>
  `subentities/${subentityName}`;
export const generalMethodPrePath = (methodName: string): string =>
  `methods/${kebab(methodName)}`;
export const getRequestPrePath = (request?: IListRequest): string => 'entity' + getQuery(request);
export const listPrePath = (request?: IListRequest): string => 'list' +  getQuery(request);
export const entityIdPrePath = (id: string): string => `entity/${id}`;
export const entityPrePath = (): string => 'entity';
