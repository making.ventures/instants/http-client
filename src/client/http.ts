import {AxiosInstance} from 'axios';
import {
  create,
  createPath,
  deleteMethod,
  deleteMethodStr,
  deletePath,
  get,
  getPath,
  getStr,
  listPath,
  list,
  uiCreate,
  uiCreatePath,
  uiDelete,
  uiDeleteStr,
  uiGet,
  uiGetPath,
  uiGetRequest,
  uiGetStr,
  uiListPath,
  uiList,
  uiUpdate,
  uiUpdatePath,
  update,
  updatePath,
  uiDeletePath,
} from './ui';
import {reprListPath, reprGetPath, reprGet, reprList} from './repr';
import {
  IUIRepresentation,
  IBaseEntity,
  RequiredId,
  TId,
  IList,
  EOrderDirection,
  TListRequest,
  TAfterRead,
  TUiUpdateMethod,
  EUiCrudMethods,
  TUiCreateMethod,
  TUiGetMethod,
  TUiGetRequestMethod,
  ECrudMethods,
  EReprMethods,
  IHttpPath,
  TUiListMethodWithPageInfo,
  TUiGetStrMethod,
  TUiDeleteMethod,
  TUiDeleteStrMethod,
  IBaseCRUDService,
  IBaseCRUDStrService,
} from '@instants/core';
import {ICache} from '../types';

export const addUiUpdate: <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize?: TAfterRead<RequiredId<TResponse>>,
) => TUiUpdateMethod<TRequest, TResponse> = <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize: TAfterRead<RequiredId<TResponse>> = (
    e: RequiredId<TResponse>,
  ) => e,
) => ({
    [EUiCrudMethods.UiUpdate]: (request: TRequest) =>
      uiUpdate<TRequest, TResponse>(getAx, basePath, request, afterDeserialize),
  });

export const addUiDelete: (
  getAx: () => AxiosInstance,
  basePath: string,
) => TUiDeleteMethod = (getAx: () => AxiosInstance, basePath: string) => ({
  [ECrudMethods.Delete]: (id: number) => uiDelete(getAx, basePath, id),
});

export const addUiDeleteStr: (
  getAx: () => AxiosInstance,
  basePath: string,
) => TUiDeleteStrMethod = (getAx: () => AxiosInstance, basePath: string) => ({
  [ECrudMethods.Delete]: (id: string) => uiDeleteStr(getAx, basePath, id),
});

export const addUiCreate: <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize?: TAfterRead<RequiredId<TResponse>>,
) => TUiCreateMethod<TRequest, TResponse> = <TRequest, TResponse>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize: TAfterRead<RequiredId<TResponse>> = (
    e: RequiredId<TResponse>,
  ) => e,
) => ({
    [EUiCrudMethods.UiCreate]: (request: TRequest) =>
      uiCreate<TRequest, TResponse>(getAx, basePath, request, afterDeserialize),
  });

export const addUiGet: <TEntity>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize?: TAfterRead<RequiredId<TEntity>>,
) => TUiGetMethod<TEntity> = <TEntity>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize: TAfterRead<RequiredId<TEntity>> = (
    e: RequiredId<TEntity>,
  ) => e,
) => ({
    [EUiCrudMethods.UiGet]: (id: number) =>
      uiGet<TEntity>(getAx, basePath, id, afterDeserialize),
  });

export const addUiGetStr: <TEntity>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize?: TAfterRead<RequiredId<TEntity>>,
) => TUiGetStrMethod<TEntity> = <TEntity>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize: TAfterRead<RequiredId<TEntity>> = (
    e: RequiredId<TEntity>,
  ) => e,
) => ({
    [EUiCrudMethods.UiGet]: (id: string) =>
      uiGetStr<TEntity>(getAx, basePath, id, afterDeserialize),
  });

export const addUiGetRequest: <TRequest, TEntity>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize?: TAfterRead<RequiredId<TEntity>>,
) => TUiGetRequestMethod<TRequest, TEntity> = <TRequest, TEntity>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize: TAfterRead<RequiredId<TEntity>> = (
    e: RequiredId<TEntity>,
  ) => e,
) => ({
    [EUiCrudMethods.UiGet]: (request: TRequest) =>
      uiGetRequest<TRequest, TEntity>(getAx, basePath, request, afterDeserialize),
  });

export const addUiList: <TRequest extends TListRequest, TListEntity>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize?: TAfterRead<RequiredId<TListEntity>>,
) => TUiListMethodWithPageInfo<TRequest, TListEntity> = <
  TRequest extends TListRequest,
  TListEntity
>(
    getAx: () => AxiosInstance,
    basePath: string,
    afterDeserialize: TAfterRead<RequiredId<TListEntity>> = (
      e: RequiredId<TListEntity>,
    ) => e,
  ) => ({
    [EUiCrudMethods.UiList]: (request?: TRequest) =>
      uiList<TListEntity, TRequest>(getAx, basePath, request, afterDeserialize),
  });

export const getReprPaths = (basePath: string) => [
  reprGetPath(basePath, '1'),
  reprListPath(basePath),
];

export const getCrudPaths = (basePath: string) => [
  getPath(basePath, '1'),
  createPath(basePath),
  deletePath(basePath, '1'),
  listPath(basePath),
  updatePath(basePath, '1'),
];

export const crudPaths: (
  basePath: string,
) => Record<ECrudMethods, IHttpPath> = (basePath) => ({
  [ECrudMethods.Get]: getPath(basePath, '1'),
  [ECrudMethods.Create]: createPath(basePath),
  [ECrudMethods.Delete]: deletePath(basePath, '1'),
  [ECrudMethods.ListWithPageInfo]: listPath(basePath),
  [ECrudMethods.Update]: updatePath(basePath, '1'),
});

export const reprPaths: (
  basePath: string,
) => Record<EReprMethods, IHttpPath> = (basePath) => ({
  [EReprMethods.ReprGet]: reprGetPath(basePath, '1'),
  [EReprMethods.ReprList]: reprListPath(basePath),
});

export const httpCrudServiceMethods: <T extends IBaseEntity<number>>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize?: TAfterRead<T>,
) => IBaseCRUDService<T> = <T extends IBaseEntity<number>>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize: TAfterRead<T> = (e: RequiredId<T>) => e,
) => ({
    [ECrudMethods.Get]: async (id: number): Promise<RequiredId<T>> =>
      afterDeserialize(await get<RequiredId<T>>(getAx, basePath, id)),

    [ECrudMethods.Create]: async (entity: T): Promise<RequiredId<T>> =>
      afterDeserialize(await create<T>(getAx, basePath, entity)),

    [ECrudMethods.Delete]: async (id: number): Promise<number> =>
      deleteMethod(getAx, basePath, id),

    [ECrudMethods.ListWithPageInfo]: async (
      request?: TListRequest<T>,
    ): Promise<IList<RequiredId<T>>> => {
      const base = await list<T>(getAx, basePath, {
        sortDirection: EOrderDirection.DESC,
        sortField: 'id',
        ...request,
      });

      return {
        pageInfo: {},
        ...base,
        list: base && base.list ? base.list.map(afterDeserialize) : [],
      };
    },

    [ECrudMethods.Update]: async (entity: T): Promise<RequiredId<T>> =>
      afterDeserialize(await update<T>(getAx, basePath, entity)),
  });

export const httpCrudServiceStrMethods: <T extends IBaseEntity<string>>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize?: TAfterRead<T>,
) => IBaseCRUDStrService<T> = <T extends IBaseEntity<string>>(
  getAx: () => AxiosInstance,
  basePath: string,
  afterDeserialize: TAfterRead<T> = (e: RequiredId<T>) => e,
) => ({
    [ECrudMethods.Get]: async (id: string): Promise<RequiredId<T>> =>
      afterDeserialize(await getStr<RequiredId<T>>(getAx, basePath, id)),

    [ECrudMethods.Create]: async (entity: T): Promise<RequiredId<T>> =>
      afterDeserialize(await create<T>(getAx, basePath, entity)),

    [ECrudMethods.Delete]: async (id: string): Promise<string> =>
      deleteMethodStr(getAx, basePath, id),

    [ECrudMethods.ListWithPageInfo]: async (
      request?: TListRequest<T>,
    ): Promise<IList<RequiredId<T>>> => {
      const base = await list<T>(getAx, basePath, {
        sortDirection: EOrderDirection.DESC,
        sortField: 'id',
        ...request,
      });

      return {
        pageInfo: {},
        ...base,
        list: base && base.list ? base.list.map(afterDeserialize) : [],
      };
    },

    [ECrudMethods.Update]: async (entity: T): Promise<RequiredId<T>> =>
      afterDeserialize(await update<T>(getAx, basePath, entity)),
  });

export const httpReprServiceMethods = <T extends TId = number>(
  getAx: () => AxiosInstance,
  basePath: string,
  cache?: ICache<IUIRepresentation<T>>,
) => ({
    reprGet: async (
      id: IUIRepresentation<T>['id'],
      request?: any,
    ): Promise<IUIRepresentation<T>> => reprGet<T>(getAx, basePath, id, cache, request),
    reprList: async (
      request: TListRequest = {
        elementsOnPage: 1000000,
      },
    ): Promise<IList<IUIRepresentation<T>>> =>
      reprList(getAx, basePath, {
        sortDirection: EOrderDirection.DESC,
        sortField: 'id',
        ...request,
      }),
  });

export const addUiGetPath: (
  basePath: string,
) => Record<EUiCrudMethods.UiGet, IHttpPath> = (basePath) => ({
  [EUiCrudMethods.UiGet]: uiGetPath(basePath, '1'),
});

export const addUiCreatePath: (
  basePath: string,
) => Record<EUiCrudMethods.UiCreate, IHttpPath> = (basePath) => ({
  [EUiCrudMethods.UiCreate]: uiCreatePath(basePath),
});

export const addUiListWithPageInfoPath: (
  basePath: string,
) => Record<EUiCrudMethods.UiList, IHttpPath> = (basePath) => ({
  [EUiCrudMethods.UiList]: uiListPath(basePath),
});

export const addUiUpdatePath: (
  basePath: string,
) => Record<EUiCrudMethods.UiUpdate, IHttpPath> = (basePath) => ({
  [EUiCrudMethods.UiUpdate]: uiUpdatePath(basePath, '1'),
});

export const addUiDeletePath: (
  basePath: string,
) => Record<ECrudMethods.Delete, IHttpPath> = (basePath) => ({
  [ECrudMethods.Delete]: uiDeletePath(basePath, '1'),
});
