import {AxiosInstance} from 'axios';
import {put, del, post, listWithPageInfo, getMWithPageInfo} from './ui';
import {listPrePath, uiPrePath} from './shared';
import {
  IBaseEntity,
  IList,
  IListRequest,
  EHttpMethod,
  IHttpPath,
  TAfterRead,
} from '@instants/core';

export const uiListSubentityPath = (
  basePath: string,
  parentId: string,
  subentityName: string,
  request?: IListRequest,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${uiPrePath()}/entity/${String(
    parentId,
  )}/subentities/${subentityName}/${listPrePath(request)}`,
});
export const uiListSubentity = async <TUIForList>(
  getAx: () => AxiosInstance,
  basePath: string,
  parentId: number,
  subentityName: string,
  request?: IListRequest,
  afterRead: (e: TUIForList) => TUIForList = (e) => e,
): Promise<IList<TUIForList>> =>
  getMWithPageInfo<IListRequest, TUIForList>(
    getAx,
    'uiListSubentity',
    uiListSubentityPath(
      basePath,
      String(parentId),
      subentityName,
      request ?
        {elementsOnPage: 1000000, ...request} :
        {elementsOnPage: 1000000},
    ).path,
    request,
    (el) => afterRead(el),
  );
export const uiListWithPageInfoSubentity = async <TUIForList>(
  getAx: () => AxiosInstance,
  basePath: string,
  parentId: number,
  subentityName: string,
  request?: IListRequest,
  afterRead: (e: TUIForList) => TUIForList = (e) => e,
): Promise<IList<TUIForList>> =>
  listWithPageInfo(
    getAx,
    'uiListSubentity',
    EHttpMethod.GET,
    uiListSubentityPath(
      basePath,
      String(parentId),
      subentityName,
      request ?
        {elementsOnPage: 1000000, ...request} :
        {elementsOnPage: 1000000},
    ).path,
    request,
    (el) => {
      // log.warn(`el: ${jstr(el)}`);

      return afterRead(el);

      // return {
      //   ...result,
      //   list: result.list ? result.list.map(el => afterRead(el)) : [],
      // };
    },
  );

export const uiUpdateSubentityPath = (
  basePath: string,
  parentId: string,
  subentityName: string,
  id: string,
): IHttpPath => ({
  method: EHttpMethod.PUT,
  path: `${basePath}/${uiPrePath()}/entity/${String(
    parentId,
  )}/subentities/${subentityName}/entity/${id}`,
});
export const uiUpdateSubentity = async <TUIUpdate extends IBaseEntity, TUIGet>(
  getAx: () => AxiosInstance,
  basePath: string,
  parentId: number,
  subentityName: string,
  entity: TUIUpdate,
  afterRead: TAfterRead<TUIGet> = (e) => e,
): Promise<TUIGet> =>
  put(
    getAx,
    'uiUpdateSubentity',
    uiUpdateSubentityPath(
      basePath,
      String(parentId),
      subentityName,
      String(entity.id),
    ).path,
    entity,
    afterRead,
  );

export const uiCreateSubentityPath = (
  basePath: string,
  parentId: string,
  subentityName: string,
): IHttpPath => ({
  method: EHttpMethod.POST,
  path: `${basePath}/${uiPrePath()}/entity/${String(
    parentId,
  )}/subentities/${subentityName}/entity`,
});
export const uiCreateSubentity = async <TUICreate, TUIGet>(
  getAx: () => AxiosInstance,
  basePath: string,
  parentId: number,
  subentityName: string,
  entity: TUICreate,
  afterRead: TAfterRead<TUIGet> = (e) => e,
): Promise<TUIGet> =>
  post(
    getAx,
    'uiCreateSubentity',
    uiCreateSubentityPath(basePath, String(parentId), subentityName).path,
    entity,
    afterRead,
  );

export const uiDeleteSubentityPath = (
  basePath: string,
  parentId: string,
  subentityName: string,
  id: string,
): IHttpPath => ({
  method: EHttpMethod.DELETE,
  path: `${basePath}/${uiPrePath()}/entity/${String(
    parentId,
  )}/subentities/${subentityName}/entity/${id}`,
});
export const uiDeleteSubentity = async <T extends IBaseEntity>(
  getAx: () => AxiosInstance,
  basePath: string,
  parentId: number,
  subentityName: string,
  id: T['id'],
): Promise<void> =>
  del(
    getAx,
    'uiDeleteSubentity',
    uiDeleteSubentityPath(basePath, String(parentId), subentityName, String(id))
      .path,
  );
