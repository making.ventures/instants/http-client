import {IBaseEntity, RequiredId} from '@instants/core';

export interface IBaseStrServiceMethods<T extends IBaseEntity<string>> {
  consistencyCheck(entity: T): Promise<void>;
  beforeCreate(entity: T): Promise<T>;
  afterRead(entity: RequiredId<T>): RequiredId<T>;
  beforeUpdate(entity: T): Promise<T>;
  couldBeDelited(entity: T): Promise<void>;
}
