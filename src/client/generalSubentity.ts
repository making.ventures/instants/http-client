import {AxiosInstance} from 'axios';
import {getM, getMWithPageInfo, post, del, put, listWithPageInfo} from './ui';
import {kebab} from 'case';
import {
  uiPrePath,
  generalSubentityPrePath,
  entityMethodPrePath,
  generalMethodPrePath,
  entityIdPrePath,
  entityPrePath,
  listPrePath,
  reprPrePath,
} from './shared';
import Cache from 'lru-cache';
import {
  IUIRepresentation,
  IBaseEntity,
  RequiredId,
  IdType,
  IList,
  IListRequest,
  IHttpPath,
  TAfterRead,
  EHttpMethod,
} from '@instants/core';

// import log from 'loglevel';
// import {jstr} from '../../utils';

export const reprGeneralSubentityListPath = (
  basePath: string,
  subEntityName: any,
  request?: IListRequest,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${reprPrePath()}/${subEntityName}/${listPrePath(request)}`,
});

export const reprGeneralSubentityList = async (
  getAx: () => AxiosInstance,
  basePath: string,
  subEntityName: any,
  request?: IListRequest,
): Promise<IList<IUIRepresentation<number>>> =>
  getMWithPageInfo<IListRequest, IUIRepresentation>(
    getAx,
    'reprGeneralSubentityList',
    reprGeneralSubentityListPath(basePath, subEntityName, request).path,
    request,
  );

export const reprGeneralSubentityGetPath = (
  basePath: string,
  subEntityName: any,
  id: any,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${reprPrePath()}/${subEntityName}/${entityIdPrePath(id)}`,
});

const cache = new Cache();

export const reprGeneralSubentityGet = async (
  getAx: () => AxiosInstance,
  basePath: string,
  subEntityName: any,
  id: number,
): Promise<IUIRepresentation> => {
  // log.warn(`reprGeneralSubentityList, id: ${id}`);

  if (!cache.has(id)) {
    // log.warn(
    //   `reprGeneralSubentityList, id: ${id}. There is no data in cache, loading`,
    // );
    const value = await getM(
      getAx,
      'reprGeneralSubentityList',
      reprGeneralSubentityGetPath(basePath, subEntityName, id).path,
    );

    // log.warn(
    //   `reprGeneralSubentityList, id: ${id}. Loaded. value: ${jstr(value)}`,
    // );
    cache.set(id, value);

    // log.warn(
    //   `reprGeneralSubentityList, id: ${id}. Put to cache, data in cache: ${jstr(
    //     cache.get(id),
    //   )}`,
    // );
  }

  return cache.get(id) as IUIRepresentation<number>;
};

export const uiGeneralSubentityMethodPath = (
  basePath: string,
  subentityName: string,
  id: string,
  methodName: string,
): IHttpPath => ({
  method: EHttpMethod.POST,
  path: `${basePath}/${uiPrePath()}/${generalSubentityPrePath(
    subentityName,
  )}/${entityMethodPrePath(id, methodName)}`,
});
export const uiGeneralSubentityMethod = async <
  T extends IBaseEntity<IdType<T>>,
  TUIRequest,
  TResponse
>(
  getAx: () => AxiosInstance,
  basePath: string,
  subentityName: string,
  id: T['id'],
  methodName: string,
  request?: TUIRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<TResponse> =>
  post(
    getAx,
    'uiGeneralSubentityMethod',
    uiGeneralSubentityMethodPath(
      basePath,
      subentityName,
      String(id),
      methodName,
    ).path,
    request,
    afterRead,
  );

export const uiGeneralSubentityGeneralMethodPath = (
  basePath: string,
  subentityName: string,
  methodName: string,
): IHttpPath => ({
  method: EHttpMethod.POST,
  path: `${basePath}/${uiPrePath()}/${generalSubentityPrePath(
    subentityName,
  )}/${generalMethodPrePath(methodName)}`,
});
export const uiGeneralSubentityGeneralMethod = async <TUIRequest, TResponse>(
  getAx: () => AxiosInstance,
  basePath: string,
  subentityName: string,
  methodName: string,
  request?: TUIRequest,
  afterRead: TAfterRead<TResponse> = (e) => e,
): Promise<TResponse> =>
  post(
    getAx,
    'uiGeneralSubentityGeneralMethod',
    uiGeneralSubentityGeneralMethodPath(basePath, subentityName, methodName)
      .path,
    request,
    afterRead,
  );

export const uiListGeneralSubentityPath = (
  basePath: string,
  subentityName: string,
  request?: IListRequest,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${uiPrePath()}/${generalSubentityPrePath(
    kebab(subentityName),
  )}/${listPrePath(request)}`,
});
export const uiListGeneralSubentity = async <
  TUIForList,
  TUIForListRequest extends IListRequest
>(
  getAx: () => AxiosInstance,
  basePath: string,
  subentityName: string,
  request?: TUIForListRequest,
  afterRead: (e: TUIForList) => TUIForList = (e) => e,
): Promise<IList<TUIForList>> =>
  getMWithPageInfo<TUIForListRequest, TUIForList>(
    getAx,
    'uiListGeneralSubentity',
    uiListGeneralSubentityPath(
      basePath,
      subentityName,
      request ?
        {elementsOnPage: 1000000, ...request} :
        {elementsOnPage: 1000000},
    ).path,
    request,
    (el) => afterRead(el),
  );
export const uiListGeneralSubentityWithPageInfo = async <
  TUIForList,
  TUIForListRequest extends IListRequest
>(
  getAx: () => AxiosInstance,
  basePath: string,
  subentityName: string,
  request?: TUIForListRequest,
  afterRead: (e: TUIForList) => TUIForList = (e) => e,
): Promise<IList<TUIForList>> =>
  getMWithPageInfo(
    getAx,
    'uiListGeneralSubentity',
    uiListGeneralSubentityPath(
      basePath,
      subentityName,
      request ?
        {elementsOnPage: 1000000, ...request} :
        {elementsOnPage: 1000000},
    ).path,
    request,
    (element) => afterRead(element),
  );

export const uiListWithPageInfoGeneralSubentity = async <
  TUIForList,
  TUIForListRequest extends IListRequest
>(
  getAx: () => AxiosInstance,
  basePath: string,
  subentityName: string,
  request?: TUIForListRequest,
  afterRead: TAfterRead<RequiredId<TUIForList>> = (e) => e,
): Promise<IList<RequiredId<TUIForList>>> =>
  listWithPageInfo(
    getAx,
    'uiListWithPageInfoGeneralSubentity',
    EHttpMethod.GET,
    uiListGeneralSubentityPath(
      basePath,
      subentityName,
      request ?
        {elementsOnPage: 1000000, ...request} :
        {elementsOnPage: 1000000},
    ).path,
    request,
    (el) => {
      // log.warn(`el: ${jstr(el)}`);

      return afterRead(el);

      // return {
      //   ...result,
      //   list: result.list ? result.list.map(el => afterRead(el)) : [],
      // };
    },
  );

export const uiUpdateGeneralSubentityPath = (
  basePath: string,
  subentityName: string,
  id: string,
): IHttpPath => ({
  method: EHttpMethod.PUT,
  path: `${basePath}/${uiPrePath()}/${generalSubentityPrePath(
    subentityName,
  )}/${entityIdPrePath(id)}`,
});
export const uiUpdateGeneralSubentity = async <
  TUIUpdate extends IBaseEntity<number | string>,
  TUIGet
>(
  getAx: () => AxiosInstance,
  basePath: string,
  subentityName: string,
  entity: TUIUpdate,
  afterRead: TAfterRead<TUIGet> = (e) => e,
): Promise<RequiredId<TUIGet>> =>
  put(
    getAx,
    'uiUpdateGeneralSubentity',
    uiUpdateGeneralSubentityPath(basePath, subentityName, String(entity.id))
      .path,
    entity,
    afterRead,
  );

export const uiGetGeneralSubentityPath = (
  basePath: string,
  subentityName: string,
  id: string,
): IHttpPath => ({
  method: EHttpMethod.GET,
  path: `${basePath}/${uiPrePath()}/${generalSubentityPrePath(
    subentityName,
  )}/${entityIdPrePath(id)}`,
});
export const uiGetGeneralSubentity = async <
  T extends IBaseEntity<IdType<T>>,
  TUIGet
>(
  getAx: () => AxiosInstance,
  basePath: string,
  subentityName: string,
  id: T['id'],
  afterRead: TAfterRead<RequiredId<TUIGet>> = (e) => e,
): Promise<RequiredId<TUIGet>> =>
  getM(
    getAx,
    'uiGetGeneralSubentity',
    uiUpdateGeneralSubentityPath(basePath, subentityName, String(id)).path,
    undefined,
    afterRead,
  );

export const uiCreateGeneralSubentityPath = (
  basePath: string,
  subentityName: string,
): IHttpPath => ({
  method: EHttpMethod.POST,
  path: `${basePath}/${uiPrePath()}/${generalSubentityPrePath(
    subentityName,
  )}/${entityPrePath()}`,
});
export const uiCreateGeneralSubentity = async <TUICreate, TUIGet>(
  getAx: () => AxiosInstance,
  basePath: string,
  subentityName: string,
  entity: TUICreate,
  afterRead: TAfterRead<TUIGet> = (e) => e,
): Promise<TUIGet> =>
  post(
    getAx,
    'uiCreateGeneralSubentity',
    uiCreateGeneralSubentityPath(basePath, subentityName).path,
    entity,
    afterRead,
  );

export const uiDeleteGeneralSubentityPath = (
  basePath: string,
  subentityName: string,
  id: string,
): IHttpPath => ({
  method: EHttpMethod.DELETE,
  path: `${basePath}/${uiPrePath()}/${generalSubentityPrePath(
    subentityName,
  )}/${entityIdPrePath(id)}`,
});

export const uiDeleteGeneralSubentity = async <T extends IBaseEntity>(
  getAx: () => AxiosInstance,
  basePath: string,
  subentityName: string,
  id: T['id'],
): Promise<void> =>
  del(
    getAx,
    'uiDeleteGeneralSubentity',
    uiDeleteGeneralSubentityPath(basePath, subentityName, String(id)).path,
  );
